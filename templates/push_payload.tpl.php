<?php
/**
 * @file
 * Template file for push payload.
 */

/**
 * Action pre & post XML content.
 */
?>
<ActionRequest xmlns="<?php print $variables["informz_url"]; ?>">
    <Brand id="<?php print $variables["informz_brand_id"]; ?>"><?php print $variables["informz_brand_name"]; ?></Brand>
    <User><?php print $variables["informz_username"]; ?></User>
    <Password><?php print $variables["informz_password"]; ?></Password>
    <Actions>
        <?php print $variables["subscribe"]; ?>
    </Actions>
</ActionRequest>
