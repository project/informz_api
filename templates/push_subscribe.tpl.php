<?php
/**
 * @file
 * Template file for subscribe.
 */

/**
 * Subscribe data XML content.
 */
?>
<Subscribe>
    <InterestDetails>
        <InterestNames>
            <InterestName><?php print $variables["uid"]; ?></InterestName>
        </InterestNames>
        <InterestAction>ReplaceAllSubscribers</InterestAction>
    </InterestDetails>
    <SubscriberData>
        <Subscribers>
            <Subscriber>
                <Email><?php print $variables["email"]; ?></Email>
                <ID><?php print $variables["uid"]; ?></ID>
            </Subscriber>
        </Subscribers>
    </SubscriberData>
</Subscribe>
