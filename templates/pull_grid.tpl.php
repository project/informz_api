<?php
/**
 * @file
 * Template file for grid.
 */

/*
 * Grid structure definition.
 */
?>
<Grid type="subscriber">
    <ReturnFields>
        <DataElement>bounce_date</DataElement>
        <DataElement>cancellation_mailing_instance_id</DataElement>
        <DataElement>cancellation_message</DataElement>
        <DataElement>cancellation_date</DataElement>
        <DataElement>email</DataElement>
        <DataElement>is_repeated_bouncer</DataElement>
        <DataElement>is_unsubscriber</DataElement>
        <DataElement>modified_date</DataElement>
        <DataElement>service_since_date</DataElement>
        <DataElement>user_id</DataElement>
    </ReturnFields>
</Grid>
