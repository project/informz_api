<?php
/**
 * @file
 * Template file for payload.
 */

/*
 * XML GridRequest.
 */
?>
<GridRequest xmlns="<?php print $variables["informz_url"]; ?>">
    <Brand id="<?php print $variables["informz_brand_id"]; ?>"><?php print $variables["informz_brand_name"]; ?></Brand>
    <User><?php print $variables["informz_username"]; ?></User>
    <Password><?php print $variables["informz_password"]; ?></Password>
    <Grids>
        <?php print $variables["grids"]; ?>
    </Grids>
</GridRequest>
