
These module allows Informz API integration, that is used to subscribe users.
Their are 2 menu callbacks for sending 
PUSH and PULL request to informz using SOAP Client.


Installation
------------

After enabling module, go to configuration page admin/config/media/informz.
Configure API related details, Username, Password, URL, etc.

For PULLING user details from informz use
http://<omain-name>/informz-api-pull-soap

For PUSHING user details to informz
http://<omain-name>/informz-api-push-soap?email_id=email@address.com&uid=timestamp

While user registration, added user insert hook to subscribe user to informz.


Author
------
Chaitanya Kulkarni
chaitanyakul@cybage.com
