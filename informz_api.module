<?php

/**
 * @file
 * Media Foundry informz_api module.
 */

/**
 * Implements hook_permission().
 */
function informz_api_permission() {
  return array(
    'access informz api' => array(
      'title' => t('Access content for the informz api module'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function informz_api_menu() {

  $items = array();
  $items['informz-api-pull-soap'] = array(
    'title' => 'Informz API configuration',
    'description' => 'Consume Informz API to PULL data',
    'page callback' => 'informz_api_pull_soap',
    'access arguments' => array('access informz api'),
  );

  $items['informz-api-push-soap'] = array(
    'title' => 'Informz API configuration',
    'description' => 'Consume Informz API to PUSH data',
    'page callback' => 'informz_api_push_soap',
    'access arguments' => array('access informz api'),
  );

  $items['admin/config/media/informz'] = array(
    'title' => 'Informz Configuration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('informz_api_config_form'),
    'access arguments' => array('administer site configuration'),
    'description' => 'Form for setting informz configuration variables',
  );

  return $items;
}

/**
 * Informz form creation.
 *
 * @param array $form
 *   Form consisting of configurable variables of informz.
 * @param array $form_state
 *   Form state by reference.
 *
 * @return string
 *   Drupal configuration form.
 */
function informz_api_config_form($form, &$form_state) {

  $form['informz_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Informz URL'),
    '#description' => t('The URL where informz request call is given.'),
    '#size' => 600,
    '#maxlength' => 400,
    '#required' => TRUE,
    '#default_value' => variable_get('informz_api_url', ''),
  );

  $form['informz_api_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Informz Username'),
    '#description' => t('Informz Username used in API call.'),
    '#size' => 600,
    '#maxlength' => 50,
    '#required' => TRUE,
    '#default_value' => variable_get('informz_api_username', ''),
  );

  $form['informz_api_password'] = array(
    '#type' => 'password',
    '#title' => t('Informz Password'),
    '#description' => t('Informz Password used in API call.'),
    '#size' => 600,
    '#maxlength' => 50,
    '#required' => TRUE,
    '#default_value' => variable_get('informz_api_password', ''),
  );

  $form['informz_api_brand_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Informz Brand ID'),
    '#description' => t('Informz Brand ID used in API call.'),
    '#size' => 600,
    '#maxlength' => 50,
    '#required' => TRUE,
    '#default_value' => variable_get('informz_api_brand_id', ''),
  );

  $form['informz_api_brand_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Informz Brand Name'),
    '#description' => t('Informz Brand Name used in API call.'),
    '#size' => 600,
    '#maxlength' => 50,
    '#required' => TRUE,
    '#default_value' => variable_get('informz_api_brand_name', ''),
  );

  return system_settings_form($form);
}

/**
 * Menu callback for pull request.
 */
function informz_api_pull_soap() {

  $grids_request = informz_api_compress(theme('grid_request_data', array()));

  $payload_vars = array(
    'informz_api_url' => variable_get('informz_api_url', 'http://partner.informz.net/aapi/2009/08/'),
    'informz_api_brand_id' => variable_get('informz_api_brand_id', '<brand_id>'),
    'informz_api_brand_name' => variable_get('informz_api_brand_name', '<brand_name>'),
    'informz_api_username' => variable_get('informz_api_username', '<username>'),
    'informz_api_password' => variable_get('informz_api_password', '<password>'),
    'grids' => $grids_request,
  );

  $xml = theme('payload_pull_data', $payload_vars);

  $soap_obj = new SoapClient('https://partner.informz.net/aapi/InformzService.svc?wsdl', array(
    'trace' => TRUE,
    'location' => variable_get("infomz_request_location", 'https://partnertest.informz.net/aapi/InformzService.svc'),
  ));
  try {
    /*
     * Here we make the service call.  The XML has to be passed as the "x"
     * parameter.
     */
    $soap_obj->PostInformzMessage(array('x' => $xml));
  }
  catch (Exception $e) {
    watchdog("informz_api", "@exception error occurred ", array("@exception" => $e->getMessage()));
  }

  /*
   * Create a DOM object and load this XML into it.  NOTE THIS IS THE
   * XML FOR THE ENTIRE SOAP RESPONSE.  Uncomment the lines to see
   * the progess.
   */
  $dom = new DOMDocument();
  $dom->loadXML($soap_obj->__getLastResponse());

  $reply = $dom->getElementsByTagName('PostInformzMessageResult')->item(0)->nodeValue;

  $replydoc = new DOMDocument();
  $replydoc->loadXML($reply);

  /*
   * Now using this subset DOM.
   * we're going to look for the <Grids> node.
   * This node should contain the
   * base64/gzip data from Informz.
   */
  $grids = $replydoc->getElementsByTagName('Grids')->item(0)->nodeValue;
  $grids = base64_decode($grids);

  /*
   * Rip off the 4-byte header.
   * You can choose to preserve this header value and
   * check it against the length of the decompressed string later
   * $gzip now contains the pure gzip compressed string
   */
  $gzip = substr($grids, 4);

  // Uncompress the string.
  $grids_data = gzdecode($gzip);

  /*
   * This uncompressed data is, itself,
   * another XML document in its own right.  We can
   * simply create a new DOM with this XML data and then use all the nodes as
   * appropriate for business logic.
   */
  $gd = new DOMDocument();
  $gd->loadXML($grids_data);

  $xml_format = $gd->saveXML();
  $xml = simplexml_load_string($xml_format);
  $json = json_encode($xml);
  $user_data = json_decode($json, TRUE);

  watchdog('informz_api', "Informz Data Synchronization Started");

  foreach ($user_data["Record"] as $user_info) {

    $user_email = $user_info["Fields"]["Field"][4];
    $form_username = explode("@", $user_email);
    $user_name = $form_username[0];

    if (user_load_by_name($user_name)) {
      watchdog("informz_api", "@user_name already exsists", array("@user_name" => $user_name));
      continue;
    }

    if (user_load_by_mail($user_email)) {
      watchdog("informz_api", "@user_email already exsists", array("@user_email" => $user_email));
      continue;
    }

    // This will generate a random password, you could set your own here.
    $password = user_password();
    // Set up the user fields.
    $fields = array(
      'name' => $user_name,
      'mail' => $user_email,
      'pass' => $password,
      'status' => 1,
      'init' => 'email address',
      'roles' => array(
        DRUPAL_AUTHENTICATED_RID => 'authenticated user',
      ),
    );

    // The first parameter is left blank so a new user is created.
    user_save('', $fields);

    watchdog("informz_api", "@user_mail account created successfully.", array("@user_mail" => $user_name));
  }
  watchdog("informz_api", "Informz Data Synchronization Completed");
  print "Informz Data Synchronization completed";
}

/**
 * Menu callback for push request.
 */
function informz_api_push_soap() {

  $params = drupal_get_query_parameters();
  $get_email = $params["email_id"];
  $get_uid = $params["uid"];

  if (empty($get_email) && empty($get_uid)) {
    print "Enter valid data!!";
    drupal_exit();
  }

  $subscriber_data = array("email" => $get_email, "uid" => $get_uid);
  // Informz subscribe data to be compressed.
  $subscribe = informz_api_compress(theme('subscribe_request_data', $subscriber_data));

  $payload_vars = array(
    'informz_api_url' => variable_get('informz_api_url', 'http://partner.informz.net/aapi/2009/08/'),
    'informz_api_brand_id' => variable_get('informz_api_brand_id', '<brand_id>'),
    'informz_api_brand_name' => variable_get('informz_api_brand_name', '<brand_name>'),
    'informz_api_username' => variable_get('informz_api_username', '<username>'),
    'informz_api_password' => variable_get('informz_api_password', '<password>'),
    'subscribe' => $subscribe,
  );

  $xml = theme('payload_push_data', $payload_vars);

  $soap_obj = new SoapClient('https://partner.informz.net/aapi/InformzService.svc?wsdl', array(
    'trace' => TRUE,
    'location' => variable_get("infomz_request_location", 'https://partnertest.informz.net/aapi/InformzService.svc'),
  ));

  try {
    /*
     * Here we make the service call.  The XML has to be passed as the "x"
     * parameter.
     */
    $soap_obj->PostInformzMessage(array('x' => $xml));
    print "Data has been written to Informz";
  }
  catch (Exception $e) {
    watchdog("informz_api", "@exception error occurred ", array("@exception" => $e->getMessage()));
    print $e->getMessage();
  }
}

/**
 * Informz defualt compression method.
 *
 * @param string $soap_obj
 *   SOAP object that will get compressed.
 *
 * @return string
 *    Base64 encoded data.
 */
function informz_api_compress($soap_obj) {

  // Save the size of the original string.
  $size = strlen($soap_obj);

  // Use gzencode to zip the original string up.
  $zip = gzencode($soap_obj);

  // The returned compressed string must start with the
  // encoded length of the ORIGINAL string.  We're going
  // to use the pack() function where "V" denotes
  // a 32-bit unsigned integer in little endian order.
  $zipper = pack('V', $size);

  // Add the compressed data after the 4-byte header.
  $zipper .= $zip;

  // $zipper now contains 4+zip data...
  // Now the zipper needs to be base64 encoded.
  $base64 = base64_encode($zipper);

  // Return the base64 encoded zipped string.
  return $base64;
}

/**
 * Implements hook_theme().
 */
function informz_api_theme($existing, $type, $theme, $path) {

  $path = drupal_get_path('module', 'informz_api');

  return array(
    'subscribe_request_data' => array(
      'template' => 'push_subscribe',
      'variables' => array('data' => NULL),
      'path' => $path . '/templates',
    ),
    'payload_push_data' => array(
      'template' => 'push_payload',
      'variables' => array('data' => NULL),
      'path' => $path . '/templates',
    ),
    'grid_request_data' => array(
      'template' => 'pull_grid',
      'variables' => array('data' => NULL),
      'path' => $path . '/templates',
    ),
    'payload_pull_data' => array(
      'template' => 'pull_payload',
      'variables' => array('data' => NULL),
      'path' => $path . '/templates',
    ),
  );
}

/**
 * Implements hook_user_insert().
 */
function informz_api_user_insert(&$edit, $account, $category) {

  $get_email = $account->mail;
  $get_uid = $account->name;

  $subscriber_data = array("email" => $get_email, "uid" => $get_uid);
  $subscribe = informz_api_compress(theme('subscribe_request_data', $subscriber_data));

  $payload_vars = array(
    'informz_api_url' => variable_get('informz_api_url', 'http://partner.informz.net/aapi/2009/08/'),
    'informz_api_brand_id' => variable_get('informz_api_brand_id', '<brand_id>'),
    'informz_api_brand_name' => variable_get('informz_api_brand_name', '<brand_name>'),
    'informz_api_username' => variable_get('informz_api_username', '<username>'),
    'informz_api_password' => variable_get('informz_api_password', '<password>'),
    'subscribe' => $subscribe,
  );

  $xml = theme('payload_push_data', $payload_vars);

  $soap_obj = new SoapClient('https://partner.informz.net/aapi/InformzService.svc?wsdl', array(
    'trace' => TRUE,
    'location' => variable_get("infomz_request_location", 'https://partnertest.informz.net/aapi/InformzService.svc'),
  ));

  try {

    $soap_obj->PostInformzMessage(array('x' => $xml));
    print "Data has been written to Informz";
  }
  catch (Exception $e) {
    watchdog("informz_api", "@exception error occurred", array("@exception" => $e->getMessage()));
    drupal_set_message($e->getMessage(), 'error');
  }
}
